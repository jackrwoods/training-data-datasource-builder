// import libraries
const xml = require('xml2json')
const csv = require('csv-parser')
const fileSystem = require('graceful-fs')

// Grab the command line arguments
const args = process.argv.slice(2) // The first two are 'node' and 'script.js'

// Create data directory if it doesn't already exist
if (!fileSystem.existsSync('data')) fileSystem.mkdirSync('data')

// Read the csv. This is built to support csvs generated by the datasource map command
const dataFileName = args[0]
const inputFileName = args[1]
let datasourceId = ''
let complete = 0 // Stores the number of data items that have been processed.
let xmlEntries = {} // Hashmap of 'Old Path' -> an array of 'Old Name'
console.log('Reading input file...')
fileSystem.createReadStream(inputFileName)
  .pipe(csv())
  .on('data', (data) => {
    // This is called for each row in the csv. It produces a JSON object that looks like:
    // {
    //   '#': '431',
    //   Type: 'StoredSignal',
    //   'Old Path': 'IHS-INDIA >>',
    //   'Old Name': 'JINDAL1.FIC580_SURGE.F_CV',
    //   'Old Description': '',
    //   'Old UOM': '',
    //   'Old ID': '0155CE7F-7EB8-42AA-85A0-5CFF50E60B87',
    //   'New Path': 'NO MATCHES',
    //   'New Name': 'NO MATCHES',
    //   'New Description': 'NO MATCHES',
    //   'New UOM': '',
    //   'New ID': 'NO MATCHES'
    // }

    // Save the datasource id once
    if (datasourceId.length === 0) datasourceId = data['Old ID']

    fileSystem.createReadStream(dataFileName).pipe(fileSystem.createWriteStream('data/' + encodeURIComponent(data['Old Name']) + '.csv'))

    if (!xmlEntries[data['Old Path']]) xmlEntries[data['Old Path']] = []
    xmlEntries[data['Old Path']].push(data['Old Name'])

    // Count this item as done
    complete++
    console.log(complete + ' complete.')
  })
  .on('end', () => {
    // Now, build the XML asset tree file.
    console.log('Done.')
    console.log('Building Asset Tree XML...')

    // Create object containing top level equipment object
    // For each level "deeper" we need to create an item containing:
    //      "level": "Level_1" or "Level_2" etc
    //      "$t": A name for this level (the ancestor name)
    //      "item": An array containing (possibly) more items
    let xmlOutputObject = {
      'equipment': {
        'item': []
      }
    }

    Object.keys(xmlEntries).forEach(ancestorPath => {
      objectReference = xmlOutputObject.equipment.item
      ancestorPathArr = ancestorPath.split('>>').map(s => s.trim()).filter(s => s.length > 0)
      ancestorPathArr.forEach((ancestorName, index) => {
        let i = objectReference.map(item => item['$t']).indexOf(ancestorName)
        if (i === -1) {
          // Ancestor must be created
          i = objectReference.push({
            'level': 'Level_' + (index + 1),
            '$t': ancestorName,
            'item': []
          })
          i-- // .push() returns the new array length, so our index is length--
        }
        if (index === ancestorPathArr.length - 1) {
          xmlEntries[ancestorPath].map(name => {
            return {
              '$t': name,
              'level': 'Level_' + (index + 2)
            }
          }).forEach(item => objectReference[i].item.push(item))
        } else {
          // Ancestor has already been added
          objectReference = objectReference[i]
        }
      })
    })

    fileSystem.writeFileSync('data/' + datasourceId + '.xml', xml.toXml(JSON.stringify(xmlOutputObject)))
    console.log('Done.')
  })
